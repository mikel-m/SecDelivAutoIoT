# FROM python:alpine3.17
# # RUN pip install flask
# # WORKDIR /app
# # COPY app.py .
# # ENTRYPOINT ["python", "app.py"]

# # Especifica la imagen base de Python
# # FROM python:3.9
# # Copia el archivo app.py al directorio /app en el contenedor
# COPY app.py /app/app.py
# # Establece el directorio de trabajo
# WORKDIR /app
# # Instala las dependencias si es necesario
# # RUN pip install <paquetes>
# # Especifica el comando por defecto para ejecutar tu aplicación
# CMD ["python", "app.py"]





# # Dockerfile
# # FROM python:3.9
# FROM python:alpine3.17

# WORKDIR /app

# COPY requirements.txt .
# RUN pip install -r requirements.txt

# COPY . .

# CMD ["python", "app.py"]





# # Docker file para API
# # Utiliza una imagen base que ya tenga Node.js instalado
# FROM node:14

# # Establece el directorio de trabajo en el directorio raíz de la aplicación
# WORKDIR /app

# # Copia el archivo server.js al directorio de trabajo
# COPY server.js .
# COPY package-lock.json .

# # Instala las dependencias necesarias (en este caso, Express)
# RUN npm install express

# # Expone el puerto en el que la aplicación estará escuchando
# EXPOSE 3000

# # Define el comando de inicio de la aplicación
# CMD ["node", "server.js"]




#######
# Docker file helloworld.py
# Utiliza la imagen base de Python
# FROM python:3.9
# Este funciona:
FROM arm64v8/python:3.13-rc-slim-bookworm
# FROM arm64v8/python:3.9.18-slim-bullseye

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia los archivos de la aplicación al directorio de trabajo
COPY helloworld.py .

# Instala las dependencias si las hay
# RUN pip install <nombre_dependencia>
RUN pip install flask

# Expone el puerto en el que la aplicación está escuchando
EXPOSE 3000

# Ejecuta el comando para iniciar la aplicación
CMD [ "python", "helloworld.py" ]