const express = require('express');
const prometheus = require('prom-client');

const app = express();
const port = 3000;

// Crea y registra tus métricas Prometheus
const counter = new prometheus.Counter({
  name: 'my_counter',
  help: 'Example counter',
});

// Incrementa el contador cada vez que se realiza una solicitud
app.use((req, res, next) => {
  counter.inc();
  next();
});

// Endpoint para exponer las métricas de Prometheus
app.get('/metrics', (req, res) => {
  res.set('Content-Type', prometheus.register.contentType);
  res.end(prometheus.register.metrics());
});

// Resto de tu código de la API
app.get('/', (req, res) => {
  res.send('¡Hola, mundo!');
});

app.listen(port, () => {
  console.log(`La API está funcionando en http://localhost:${port}`);
});