from flask import Flask
import sys

app = Flask(__name__)

# Endpoint principal
@app.route('/')
def hello():
    return '¡Hola, mundo!'

# Endpoint para verificar el estado de salud
@app.route('/health')
def health_check():
    # Comprueba cualquier condición de salud aquí
    # Si todas las condiciones son correctas, devuelve un código de estado 200 "OK"
    # De lo contrario, devuelve un código de estado 500 "Internal Server Error"
    # Puedes personalizar las condiciones según las necesidades de tu aplicación
    if all_conditions_are_met():
        return '', 200
    else:
        return '', 500

def all_conditions_are_met():
    # Aquí puedes implementar tus propias condiciones de salud
    # Por ejemplo, verifica si todas las conexiones a bases de datos están activas,
    # si los servicios externos están disponibles, etc.
    # Si todas las condiciones son correctas, devuelve True; de lo contrario, False.
    return True

if __name__ == '__main__':
    # Obtén el puerto del argumento de línea de comandos o utiliza un valor predeterminado
    port = int(sys.argv[1]) if len(sys.argv) > 1 else 3000
    # Inicia la aplicación Flask en el puerto especificado
    app.run(port=port)